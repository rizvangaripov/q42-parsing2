package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;
import javafx.util.Pair;

import java.awt.*;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        Factory factory = createFactory();

        File folder = new File(factoryDataDirectoryPath);
        ArrayList<String[]> values = new ArrayList<>();
        for (File file : folder.listFiles()) {
            FileReader fileReader = null;
            try {
                fileReader = new FileReader(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            try {
                bufferedReader.readLine();
                String string = "";
                string = bufferedReader.readLine();
                while (!string.equals("---")) {
                    values.add(string.split(":"));
                    string = bufferedReader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        setValues(factory, values);
        return factory;
    }

    public Factory createFactory() {
        Class<Factory> factoryClass = Factory.class;
        Factory factory = null;
        try {
            Constructor<Factory> constructor = factoryClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            factory = constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                InvocationTargetException e) {
            e.printStackTrace();
        }
        return factory;
    }

    public void setValues(Factory factory, ArrayList<String[]> values) {
        for (Field field : Factory.class.getDeclaredFields()) {
            Concatenate concatenate = field.getAnnotation(Concatenate.class);
            if (concatenate != null) {
                continue;
            }
            NotBlank notBlank = field.getAnnotation(NotBlank.class);
            if (notBlank != null) {
                if (getValue(field.getName(), values) != null && getValue(field.getName(), values).length > 1 &&
                        !getValue(field.getName(), values)[1].contains("null")) {
                    field.setAccessible(true);
                    try {
                        field.set(factory, getValue(field.getName(), values)[1]);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                continue;
            }
            if (field.getType() == String.class) {
                if (getValue(field.getName(), values) != null) {
                    field.setAccessible(true);
                    try {
                        field.set(factory, getValue(field.getName(), values)[1]);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (field.getType() == Long.class) {
                if (getValue(field.getName(), values) != null) {
                    field.setAccessible(true);
                    String[] strings = getValue(field.getName(), values)[1].split("\"");
                    try {
                        field.set(factory, strings[1]);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (field.getType() == List.class) {
                if (getValue(field.getName(), values) != null) {
                    field.setAccessible(true);
                    String[] strings = getValue(field.getName(), values)[1].split(",");
                    try {
                        field.set(factory, strings);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public String[] getValue(String value, ArrayList<String[]> values) {
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i)[0].contains(value)) {
                return values.get(i);
            }
        }
        return null;
    }
}
